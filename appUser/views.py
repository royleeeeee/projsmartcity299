from django.http import *
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout as d_logout
from . import forms

# Validates user credentials
def signin(request):
	username = ''
	password = ''
	if request.POST:
		username = request.POST['form-username']
		password = request.POST['form-password']
		
		user = authenticate(username=username, password=password)
		if user is not None:
			login(request, user)
			return HttpResponseRedirect('index/')
		else:
			return HttpResponseRedirect('/')
	
	return render(request, 'appUser/signin.html')

# Register user function
def register(request):
		
	return render(request, 'appUser/register.html')
	
@login_required
def logout(request):
	d_logout(request)
	return render(request, 'appUser/logout.html')
