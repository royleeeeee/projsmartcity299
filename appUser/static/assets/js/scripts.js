
jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
	//{% load staticfiles %}
    $.backstretch("../../static/assets/img/backgrounds/bris.jpg");
    
    /*
        Form validation for login
    */
    $('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    $('.login-form').on('submit', function(e) {
    	
    	$(this).find('input[type="text"], input[type="password"], textarea').each(function(){
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	
    });
	
	/*
        Form validation for register
    */
    $('.register-form input[type="text"], .register-form input[type="password"], .register-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    $('.register-form').on('submit', function(e) {
    	
    	$(this).find('input[name="form-name"], input[name="form-username"], input[name="form-password"], input[name="form-password2"], textarea').each(function(){
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	
    });
    
    
});
