from django.conf.urls import url, include
from . import views

urlpatterns = [
	# Render signin page
	url(r'^$', views.signin, name='signin'),
	
	# Render register page
	url(r'^register/$', views.register, name='register'),
	
	# Render logout page
	url(r'^logout/$', views.logout, name='logout'),
]