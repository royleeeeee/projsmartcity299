from django.db import models

defLongtitude = 'asd'
defLatitude = 'asd'

# Database model for tables
class College(models.Model):
    name = models.CharField(max_length=200, blank=True, default='')
    address = models.CharField(max_length=200, blank=True, default='')
    departments = models.CharField(max_length=200, blank=True, default='')
    email = models.CharField(max_length=200, blank=True, default='')
    latitude = models.CharField(max_length=100, default='0')
    longitude = models.CharField(max_length=100, default='0')
    def __str__(self):
        return self.name

class Library(models.Model):
    name = models.CharField(max_length=200, blank=True, default='')
    address = models.CharField(max_length=200, blank=True, default='')
    phoneNumbers = models.CharField(max_length=15, blank=True, default='')
    email = models.CharField(max_length=200, blank=True, default='')
    latitude = models.CharField(max_length=100, default='0')
    longitude = models.CharField(max_length=100, default='0')
    def __str__(self):
        return self.name

class Industry(models.Model):
    name = models.CharField(max_length=200, blank=True, default='')
    address = models.CharField(max_length=200, blank=True, default='')
    industryType = models.CharField(max_length=200, blank=True, default='')
    email = models.CharField(max_length=200, blank=True, default='')
    latitude = models.CharField(max_length=100, default='0')
    longitude = models.CharField(max_length=100, default='0')
    def __str__(self):
        return self.name

class Hotel(models.Model):
    name = models.CharField(max_length=200, blank=True, default='')
    address = models.CharField(max_length=200, blank=True, default='')
    phoneNumbers = models.CharField(max_length=15, blank=True, default='')
    email = models.CharField(max_length=200, blank=True, default='')
    latitude = models.CharField(max_length=100, default='0')
    longitude = models.CharField(max_length=100, default='0')
    def __str__(self):
        return self.name

class Park(models.Model):
    name = models.CharField(max_length=200, blank=True, default='')
    address = models.CharField(max_length=200, blank=True, default='')
    phoneNumbers = models.CharField(max_length=15, blank=True, default='')
    email = models.CharField(max_length=200, blank=True, default='')
    latitude = models.CharField(max_length=100, default='0')
    longitude = models.CharField(max_length=100, default='0')
    def __str__(self):
        return self.name

class Zoo(models.Model):
    name = models.CharField(max_length=200, blank=True, default='')
    address = models.CharField(max_length=200, blank=True, default='')
    phoneNumbers = models.CharField(max_length=15, blank=True, default='')
    email = models.CharField(max_length=200, blank=True, default='')
    latitude = models.CharField(max_length=100, default='0')
    longitude = models.CharField(max_length=100, default='0')
    def __str__(self):
        return self.name

class Museum(models.Model):
    name = models.CharField(max_length=200, blank=True, default='')
    address = models.CharField(max_length=200, blank=True, default='')
    phoneNumbers = models.CharField(max_length=15, blank=True, default='')
    email = models.CharField(max_length=200, blank=True, default='')
    latitude = models.CharField(max_length=100, default='0')
    longitude = models.CharField(max_length=100, default='0')
    def __str__(self):
        return self.name

class Restaurant(models.Model):
    name = models.CharField(max_length=200, blank=True, default='')
    address = models.CharField(max_length=200, blank=True, default='')
    phoneNumbers = models.CharField(max_length=15, blank=True, default='')
    email = models.CharField(max_length=200, blank=True, default='')
    latitude = models.CharField(max_length=100, default='0')
    longitude = models.CharField(max_length=100, default='0')
    def __str__(self):
        return self.name

class Mall(models.Model):
    name = models.CharField(max_length=200, blank=True, default='')
    address = models.CharField(max_length=200, blank=True, default='')
    phoneNumbers = models.CharField(max_length=15, blank=True, default='')
    email = models.CharField(max_length=200, blank=True, default='')
    latitude = models.CharField(max_length=100, default='0')
    longitude = models.CharField(max_length=100, default='0')
    def __str__(self):
        return self.name
