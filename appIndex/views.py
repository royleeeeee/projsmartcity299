from django.shortcuts import render
from django.views.generic import ListView

# Render contact page
def contact(request):
    return render(request, 'appIndex/contact.html')

# Render homepage
def home(request):
    return render(request, 'appIndex/home.html')