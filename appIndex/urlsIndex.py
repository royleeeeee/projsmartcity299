from django.conf.urls import url, include
from django.views.generic import ListView, DetailView, TemplateView
from . import views
from appIndex.models import *

urlpatterns = [
	# Render homepage
	url(r'^$', views.home, name='home'),
	
	# Render contact page
	url(r'^contact/$', views.contact, name='contact'),

	# Render selected page and pass data to table
	url(r'^college/$', ListView.as_view(queryset=College.objects.all(),
										template_name='appIndex/college.html')),

	url(r'^library/$', ListView.as_view(queryset=Library.objects.all(),
										template_name='appIndex/library.html')),

	url(r'^industry/$', ListView.as_view(queryset=Industry.objects.all(),
										template_name='appIndex/industry.html')),

	url(r'^hotel/$', ListView.as_view(queryset=Hotel.objects.all(),
										template_name='appIndex/hotel.html')),

	url(r'^park/$', ListView.as_view(queryset=Park.objects.all(),
										template_name='appIndex/park.html')),

	url(r'^zoo/$', ListView.as_view(queryset=Zoo.objects.all(),
										template_name='appIndex/zoo.html')),

	url(r'^museum/$', ListView.as_view(queryset=Museum.objects.all(),
										template_name='appIndex/museum.html')),

	url(r'^restaurant/$', ListView.as_view(queryset=Restaurant.objects.all(),
										template_name='appIndex/restaurant.html')),

	url(r'^mall/$', ListView.as_view(queryset=Mall.objects.all(),
										template_name='appIndex/mall.html')),

]