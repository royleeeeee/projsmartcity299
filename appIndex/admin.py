from django.contrib import admin
from .models import *

# Registers database model from model.py to admin interface
# to allow admins to modify data
admin.site.register(College)
admin.site.register(Library)
admin.site.register(Industry)
admin.site.register(Hotel)
admin.site.register(Park)
admin.site.register(Zoo)
admin.site.register(Museum)
admin.site.register(Restaurant)
admin.site.register(Mall)